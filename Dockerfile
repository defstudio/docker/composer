FROM composer:latest

RUN set -eux;
RUN apk add --no-cache --virtual .build-deps libxml2-dev

RUN docker-php-ext-install pcntl
RUN docker-php-ext-install soap
RUN docker-php-ext-install exif

RUN apk add --no-cache --update --virtual .build-deps autoconf automake gawk build-base

RUN apk add --no-cache freetype libpng libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev && \
  docker-php-ext-configure gd \
    --with-freetype \
    --with-jpeg && \
  NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
  docker-php-ext-install -j${NPROC} gd && \
  apk del --no-cache freetype-dev libpng-dev libjpeg-turbo-dev

RUN apk add --no-cache --update --virtual .build-deps autoconf automake gawk build-base


RUN pecl install -o -f redis
RUN rm -rf /tmp/pear
RUN docker-php-ext-enable redis
